grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog 
    : (stat | block)+ EOF!;

block 
    : LB^ NL! (stat | block)* RB!;

stat
    : expr 
    
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
    
if_expr
    : expr
      ( EQ^ expr
      | NE^ expr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

if_stat
    : IF^ if_expr THEN! block (ELSE! block)?
    ;

  
VAR 
    : 'var';

IF
    : 'if'
    ;
    
ELSE
    : 'else'
    ;
  
THEN
    : 'then'
    ;
  
EQ
    : '=='
    ;
  
NE
    : '!='
    ;
  
ID 
    : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT 
    : '0'..'9'+
    ;

NL 
    : '\r'? '\n' 
    ;

WS 
    : (' ' | '\t')+ {$channel = HIDDEN;} 
    ;

LP
    : '('
    ;

RP
    : ')'
    ;

LB 
    : '{'
    ; 
  
RB
    : '}'
    ;
    
PODST
    : '='
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MUL
    : '*'
    ;

DIV
    : '/'
    ;
  